# RELEASE NOTES: *frame*, A utility displaying in a frame what is read on stdin.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.6**:
  - Updated build system components.

- **Version 1.1.5**:
  - Updated build system.

- **Version 1.1.4**:
  - Removed unused files.

- **Version 1.1.3**:
  - Updated build system component(s)

- **Version 1.1.2**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.1**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.0.0**:
  - Added alignment options.

- **Version 1.0.0**:
  - First version.
  - **DOES NOT** run under Windows. The getline() function coming from linux platform's libc is to be reimplemented...
  