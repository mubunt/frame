 # *frame*, A utility displaying in a frame what is read on stdin.

This utility is based on the *libFrame* library to find substantially at the same location.

## LICENSE
**frame** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ frame --help
frame - Copyright (c) 2019, Michel RIZZO. All Rights Reserved.
frame - Version 1.0.0

A utility displaying in a frame what is read on stdin

Usage: frame [OPTIONS]...

 -h, --help     Print help and exit
  -V, --version  Print version and exit
  -n, --no       No frame  (default=off)
  -t, --text     Text frame  (default=off)
  -s, --single   Single frame  (default=off)
  -d, --double   Double frame  (default=off)
  -b, --block    Block frame  (default=off)
  -B, --bold     Bold  (default=off)
  -l, --left     Left align.  (default=off)
  -c, --center   Centered  (default=off)
  -r, --right    Right align  (default=off)

Exit: returns a non-zero status if an error is detected.

$ frame --version
frame - Copyright (c) 2019, Michel RIZZO. All Rights Reserved.
frame - Version 1.0.0
$ cat RELEASENOTES.md | frame --text

+------------------------------------------------------------------------------------------------------------------------+
| # RELEASE NOTES: *frame*, A utility displaying in a frame what is read on stdin.                                       |
|                                                                                                                        |
| Functional limitations, if any, of this version are described in the *README.md* file.                                 |
|                                                                                                                        |
| **Version 1.0.0**:                                                                                                     |
|   - First version.                                                                                                     |
|   - **DOES NOT** run under Windows. The getline() function coming from linux platform's libc is to be reimplemented... |
|                                                                                                                        |
+------------------------------------------------------------------------------------------------------------------------+

$ ls | frame --double --bold
...

```
## STRUCTURE OF THE APPLICATION
This section walks you through **frame**'s structure. Once you understand this structure, you will easily find your way around in **frame**'s code base.

``` bash
$ yaTree
./                  # Application level
├── src/            # Source directory
│   ├── Makefile    # Makefile
│   ├── frame.c     # Source file
│   └── frame.ggo   # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md      # GNU General Public License markdown file
├── LICENSE.md      # License markdown file
├── Makefile        # Makefile
├── README.md       # ReadMe markdown file
├── RELEASENOTES.md # Release Notes markdown file
└── VERSION         # Version identification text file

1 directories, 9 files
$ 
```

## HOW TO BUILD THIS APPLICATION ON LINUX
```bash
$ cd frame
$ make clean all
```

## HOW TO BUILD THIS APPLICATION ON LINUX FOR WINDOWS
```bash
$ cd frame
$ make wclean wall
```

## HOW TO INSTALL AND USE THIS APPLICATION ON LINUX
```Shell
$ cd frame
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For development:
    - *mingw (C compiler)* packages to build for Windows platform.
- For usage, nothing particular...
- Developped and tested on XUBUNTU 19.10, GCC v9.2.1 20191008

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***