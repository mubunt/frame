//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: frame
// A utility displaying in a frame what is read on stdin
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_cmdline.h"
#include "frame.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define error(fmt, ...) \
	do { fprintf(stderr, "ERROR: " fmt "\n", __VA_ARGS__); } while (0)
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
//------------------------------------------------------------------------------
// LOCAL ROUTINES
//------------------------------------------------------------------------------
#ifndef LINUX
ssize_t getline(char **lineptr, size_t *n, FILE *stream) {
	// TO BE WRITTEN
	return -1;
}
#endif
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	unsigned short ftype;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_frame(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	unsigned int n = args_info.no_given + args_info.text_given + args_info.single_given + args_info.double_given + args_info.block_given;
	if (n != 0 && n!= 1) {
		error("%s", "Multiple frame options. Choose one.");
		goto EXIT;
	}
	n = args_info.left_given + args_info.center_given + args_info.right_given;
	if (n != 0 && n!= 1) {
		error("%s", "Multiple alignment options. Choose one.");
		goto EXIT;
	}
	ftype = SINGLEFRAME;
	if (args_info.no_given) ftype = NOFRAME;
	if (args_info.text_given) ftype = TEXTFRAME;
	if (args_info.single_given) ftype = SINGLEFRAME;
	if (args_info.double_given) ftype = DOUBLEFRAME;
	if (args_info.block_given) ftype = BLOCKFRAME;
	if (args_info.bold_given) ftype |= BOLDFRAME;
	if (args_info.left_given) ftype |= LEFTFRAME;
	if (args_info.center_given) ftype |= CENTERFRAME;
	if (args_info.right_given) ftype |= RIGHTFRAME;
	//----  Go on --------------------------------------------------------------
	struct sFrame *fr1;
	char *line = NULL;
	size_t len = 0;
	fr1 = frame_ini();
	while (getline(&line, &len, stdin) != -1)
		frame_add(fr1, "%s", line);
	free(line);
	if (! frame_testWidth(fr1, ftype)) {
		error("%s", "The window is NOT wide enough to display");
		frame_abort(fr1);
		goto EXIT;
	}
	frame_close(fr1, stdout, ftype);
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_frame_free(&args_info);
	return EXIT_SUCCESS;
EXIT:
	cmdline_parser_frame_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
